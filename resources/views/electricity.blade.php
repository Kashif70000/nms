@extends('site_layout')@section('body-section')
  <!-- Content
  ============================================= -->
  <div id="content">
    
    <!-- Secondary Navigation
    ============================================= -->
    <div class="bg-secondary">
      <div class="container">
        <ul class="nav secondary-nav">
          <li class="nav-item"> <a class="nav-link" href="/"><span><i class="fas fa-mobile-alt"></i></span> Airtime</a> </li>
          <li class="nav-item"> <a class="nav-link" href="/tv"><span><i class="fas fa-tv"></i></span> TV</a> </li>
          <li class="nav-item"> <a class="nav-link" href="/data"><span><i class="fas fa-wifi"></i></span> Data</a> </li>
          <li class="nav-item"> <a class="nav-link" href="/free-tv"><span><i class="fas fa-desktop"></i></span> FreeTV</a> </li>
          <li class="nav-item"> <a class="nav-link active" href="/electricity"><span><i class="fas fa-lightbulb"></i></span> Electricity</a> </li>
        </ul>
      </div>
    </div><!-- Secondary Navigation end -->
    
    <section class="container">
      <div class="bg-light shadow-md rounded p-4">
        <div class="row">
        
          <!-- Electricity Bill
          ============================================= -->
          <div class="col-lg-4 mb-4 mb-lg-0">
            <h2 class="text-4 mb-3">Buy electricity Tokens</h2>
            <form id="electricityBill" method="post">
              <span class="sub-heading">Select your Provider</span>
              <ul class="billing-methods">
                <li>
                  <label for="eko">
                    <input type="radio" id="eko" name="method">
                    <span class="image-holder">
                      <span class="img"><img src="/frontend/images/eko.jpg" alt="eko"></span>
                      <span class="text">Eko</span>
                    </span>
                  </label>
                </li>
                <li>
                  <label for="ikeja">
                    <input type="radio" id="ikeja" name="method">
                    <span class="image-holder">
                      <span class="img"><img src="/frontend/images/ikeja.jpeg" alt="ikeja"></span>
                      <span class="text">Ikeja</span>
                    </span>
                  </label>
                </li>
                <li>
                  <label for="kano">
                    <input type="radio" id="kano" name="method">
                    <span class="image-holder">
                      <span class="img"><img src="/frontend/images/kano.jpeg" alt="kano"></span>
                      <span class="text">Kano</span>
                    </span>
                  </label>
                </li>
                <li>
                  <label for="abuja">
                    <input type="radio" id="abuja" name="method">
                    <span class="image-holder">
                      <span class="img"><img src="/frontend/images/abuja.png" alt="Abuja"></span>
                      <span class="text">Abuja</span>
                    </span>
                  </label>
                </li>
                <li>
                  <label for="ibadan">
                    <input type="radio" id="ibadan" name="method">
                    <span class="image-holder">
                      <span class="img"><img src="/frontend/images/ibadan.png" alt="ibadan"></span>
                      <span class="text">Ibadan</span>
                    </span>
                  </label>
                </li>
                <li>
                  <label for="enugu">
                    <input type="radio" id="enugu" name="method">
                    <span class="image-holder">
                      <span class="img"><img src="/frontend/images/enugu.png" alt="enugu"></span>
                      <span class="text">Enugu</span>
                    </span>
                  </label>
                </li>
              </ul>
              <div class="form-group">
                <input type="text" class="form-control" data-bv-field="number" id="serviceNumber" required placeholder="Enter Meter number">
              </div>
              <div class="form-group">
                <input type="text" class="form-control" id="name" required placeholder="Here Name will be shown">
              </div>
              <div class="form-group input-group">
                <div class="input-group-prepend"> <span class="input-group-text">₦</span> </div>
                <input class="form-control" id="amount" placeholder="Enter Amount" required type="text">
              </div>
              <button class="btn btn-primary btn-block" type="submit">Continue</button>
            </form>
          </div><!-- Electricity Bill end -->
          
          <!-- Slideshow
          ============================================= -->
          <div class="col-lg-8">
            <div class="owl-carousel owl-theme slideshow single-slider">
              <div class="item"><a href="#"><img class="img-fluid" src="/frontend/images/slider/banner-6.jpg" alt="banner 5" /></a></div>
            </div>
          </div><!-- Slideshow end -->
          
        </div>
      </div>
    </section>
    
    <!-- Tabs
    ============================================= -->
    <div class="section pt-4 pb-3 d-none">
      <div class="container">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
          <li class="nav-item"> <a class="nav-link active" id="mobile-recharge-tab" data-toggle="tab" href="#mobile-recharge" role="tab" aria-controls="mobile-recharge" aria-selected="true">Electricity Bill Payment</a> </li>
          <li class="nav-item"> <a class="nav-link" id="billpayment-tab" data-toggle="tab" href="#billpayment" role="tab" aria-controls="billpayment" aria-selected="false">Best Offers</a> </li>
          <li class="nav-item"> <a class="nav-link" id="why-quickai-tab" data-toggle="tab" href="#why-quickai" role="tab" aria-controls="why-quickai" aria-selected="false">Pay Online</a> </li>
        </ul>
        <div class="tab-content my-3" id="myTabContent">
          <div class="tab-pane fade show active" id="mobile-recharge" role="tabpanel" aria-labelledby="mobile-recharge-tab">
            <p>Instant Online CableTv Bill Payment Iisque persius interesset his et, in quot quidam persequeris vim, ad mea essent possim iriure. Mutat tacimates id sit. Ridens mediocritatem ius an, eu nec magna imperdiet. Mediocrem qualisque in has. Enim utroque perfecto id mei, ad eam tritani labores facilisis, ullum sensibus no cum. Eius eleifend in quo. At mei alia iriure propriae.</p>
            <p>Partiendo voluptatibus ex cum, sed erat fuisset ne, cum ex meis volumus mentitum. Alienum pertinacia maiestatis ne eum, verear persequeris et vim. Mea cu dicit voluptua efficiantur, nullam labitur veritus sit cu. Eum denique omittantur te, in justo epicurei his, eu mei aeque populo. Cu pro facer sententiae, ne brute graece scripta duo. No placerat quaerendum nec, pri alia ceteros adipiscing ut. Quo in nobis nostrum intellegebat. Ius hinc decore erroribus eu, in case prima exerci pri. Id eum prima adipisci. Ius cu minim theophrastus, legendos pertinacia an nam.</p>
            </div>
          <div class="tab-pane fade" id="billpayment" role="tabpanel" aria-labelledby="billpayment-tab">
            <p>Partiendo voluptatibus ex cum, sed erat fuisset ne, cum ex meis volumus mentitum. Alienum pertinacia maiestatis ne eum, verear persequeris et vim. Mea cu dicit voluptua efficiantur, nullam labitur veritus sit cu. Eum denique omittantur te, in justo epicurei his, eu mei aeque populo. Cu pro facer sententiae, ne brute graece scripta duo. No placerat quaerendum nec, pri alia ceteros adipiscing ut. Quo in nobis nostrum intellegebat. Ius hinc decore erroribus eu, in case prima exerci pri. Id eum prima adipisci. Ius cu minim theophrastus, legendos pertinacia an nam.</p>
            <p>Instant Online mobile recharge Iisque persius interesset his et, in quot quidam persequeris vim, ad mea essent possim iriure. Mutat tacimates id sit. Ridens mediocritatem ius an, eu nec magna imperdiet. Mediocrem qualisque in has. Enim utroque perfecto id mei, ad eam tritani labores facilisis, ullum sensibus no cum. Eius eleifend in quo. At mei alia iriure propriae.</p>
          </div>
          <div class="tab-pane fade" id="why-quickai" role="tabpanel" aria-labelledby="why-quickai-tab">
            <p>Cu pro facer sententiae, ne brute graece scripta duo. No placerat quaerendum nec, pri alia ceteros adipiscing ut. Quo in nobis nostrum intellegebat. Ius hinc decore erroribus eu, in case prima exerci pri. Id eum prima adipisci. Ius cu minim theophrastus, legendos pertinacia an nam.</p>
            <p>Partiendo voluptatibus ex cum, sed erat fuisset ne, cum ex meis volumus mentitum. Alienum pertinacia maiestatis ne eum, verear persequeris et vim. Mea cu dicit voluptua efficiantur, nullam labitur veritus sit cu. Eum denique omittantur te, in justo epicurei his, eu mei aeque populo.</p>
            <p>Instant Online mobile recharge Iisque persius interesset his et, in quot quidam persequeris vim, ad mea essent possim iriure. Mutat tacimates id sit. Ridens mediocritatem ius an, eu nec magna imperdiet. Mediocrem qualisque in has. Enim utroque perfecto id mei, ad eam tritani labores facilisis, ullum sensibus no cum. Eius eleifend in quo. At mei alia iriure propriae.</p>
          </div>
        </div>
      </div>
    </div><!-- Tabs end -->
    
    <!-- Refer & Earn
    ============================================= -->
    <div class="container d-none">
      <section class="section bg-light shadow-md rounded px-5">
        <h2 class="text-9 font-weight-600 text-center">Refer & Earn</h2>
        <p class="lead text-center mb-5">Refer your friends and earn up to $20.</p>
        <div class="row">
          <div class="col-md-4">
            <div class="featured-box style-4">
              <div class="featured-box-icon bg-light-4 text-primary rounded-circle"> <i class="fas fa-bullhorn"></i> </div>
              <h3>You Refer Friends</h3>
              <p class="text-3">Share your referral link with friends. They get $10.</p>
            </div>
          </div>
          <div class="col-md-4">
            <div class="featured-box style-4">
              <div class="featured-box-icon bg-light-4 text-primary rounded-circle"> <i class="fas fa-sign-in-alt"></i> </div>
              <h3>Your Friends Register</h3>
              <p class="text-3">Your friends Register with using your referral link.</p>
            </div>
          </div>
          <div class="col-md-4">
            <div class="featured-box style-4">
              <div class="featured-box-icon bg-light-4 text-primary rounded-circle"> <i class="fas fa-dollar-sign"></i> </div>
              <h3>Earn You</h3>
              <p class="text-3">You get $20. You can use these credits to take recharge.</p>
            </div>
          </div>
        </div>
        <div class="text-center pt-4"> <a href="#" class="btn btn-primary">Get Started Earn</a> </div>
      </section>
    </div><!-- Refer & Earn end -->
    
    <!-- Mobile App
    ============================================= -->
    <section class="section pb-0">
      <div class="container">
        <div class="row">
          <div class="col-md-5 col-lg-6 text-center"> <img class="img-fluid" alt="" src="/frontend/images/app-mobile.png"> </div>
          <div class="col-md-7 col-lg-6">
            <h2 class="text-9 font-weight-600 my-4">Download Our PayHere<br class="d-none d-lg-inline-block">
              Mobile App Now</h2>
            <p class="lead">Download our app for the fastest, most convenient way to send Recharge.</p>
            <p>Ridens mediocritatem ius an, eu nec magna imperdiet. Mediocrem qualisque in has. Enim utroque perfecto id mei, ad eam tritani labores facilisis, ullum sensibus no cum. Eius eleifend in quo.</p>
            <ul>
              <li>Recharge</li>
              <li>Bill Payment</li>
              <li>Booking Online</li>
              <li>and much more.....</li>
            </ul>
            <div class="d-flex flex-wrap pt-2"> <a class="mr-4" href=""><img alt="" src="/frontend/images/app-store.png"></a><a href=""><img alt="" src="/frontend/images/google-play-store.png"></a> </div>
          </div>
        </div>
      </div>
    </section><!-- Mobile App end -->
    
  </div><!-- Content end -->
  
  
  @stop