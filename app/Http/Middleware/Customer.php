<?php

namespace App\Http\Middleware;

use Closure;
class Customer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    
    public function handle($request, Closure $next ,$guard=null)
    {
        if (\Auth::guard($guard)->check()) {
          
            if(\Auth::guard($guard)->user()->user_type == 'customer' )
            {
                
                return $next($request);
            }
            
            else
            {
                $error_heading = "Oops ! Unauthorized ";
                $error_description = "This page does not exit or You are not authorized to view this page.";
                $class = 'danger';
                return response(view('error-page',compact('error_heading','error_description','class')));
               return $error_heading;
            }
           
        }
        
        return redirect()->guest(route('login'));
    }
}
