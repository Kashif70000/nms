<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/admin', function () {
    
    if (Auth::user()) {
       
        if(Auth::user()->user_type == 'admin'){
            return redirect()->intended('admin/dashboard');
        }else{
            return redirect()->intended('/customer');
        }
       
        

    } else {
            
        return redirect()->intended('/login');
    }
});

Auth::routes();

Route::get('admin/dashboard', 'AdminController@index');
// Route::get('/admin/users', 'AdminController@users');
// Route::get('/admin/add-user', 'AdminController@addUser');
// Route::post('/admin/create_user', 'AdminController@createUser');
// Route::get('/admin/edit-user/{user_id}', 'AdminController@editUser');
// Route::post('/admin/update-user/{user_id}', 'AdminController@updateUser');

// Route::get('/admin/delete-user/{user_id}','AdminController@deleteUser');
// Route::get('/admin/user-detail/{user_id}','AdminController@userDetail');
// Route::get('/admin/user_status/{user_id}/{status}','AdminController@editUserStatus');
Route::get('/customer', 'Customer@index');
Route::get('/', function () {
    return view('index');
});

Route::get('/tv', function () {
    return view('tv');
});

Route::get('/data', function () {
    return view('data');
});

Route::get('/free-tv', function () {
    return view('free_tv');
});

Route::get('/electricity', function () {
    return view('electricity');
});


